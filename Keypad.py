import RPi.GPIO as GPIO
import time

#GPIO Setup
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#Setup Keypad
MATRIX = [ [1,2,3,'A'],
           [4,5,6,'B'],
           [7,8,9,'C'],
           ['*',0,'#','D'] ]

#GPIO Pin Setup
ROW = [2,3,4,17]
COL = [27,22,10,9]


for j in range(4):
        GPIO.setup(COL[j], GPIO.OUT)
        GPIO.output(COL[j], 1)

for i in range(4):
    GPIO.setup(ROW[i], GPIO.IN, pull_up_down = GPIO.PUD_UP)

def check_keypad(length):
    ROW = [2,3,4,17]
    COL = [27,22,10,9]
    
    MATRIX = [ [1,2,3,'A'],
           [4,5,6,'B'],
           [7,8,9,'C'],
           ['*',0,'#','D'] ]
    result = ""
    while(True):
            for j in range(4):
                GPIO.output(COL[j],0) 
                
                for i in range(4):
                    if GPIO.input(ROW[i]) == 0:
                        time.sleep(0.3)
                        print(MATRIX[i][j])
                        result = result + str(MATRIX[i][j])
                        time.sleep(0.3)
                        while(GPIO.input(ROW[i]) == 0):
                            pass
                
                GPIO.output(COL[j],1)              
                if len(result) >= length:
                    return result

#Password
password = "1A4B"
length = len(password)

#Password from keypad
print("Please Enter a user password")
result = check_keypad(length)

#Password Check
if result == password:
    print("\nCorrect! Unlocked")
else:
    print("\nIncorrect Passcode")
    

    
