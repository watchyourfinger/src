#This program will take a photo and save it
#The photo will have a file name of the date of when the photo was taken

import subprocess
import datetime

date_object = datetime.date.today()

cmd = 'raspistill -f -o %s.jpg' % (date_object)
subprocess.call(cmd, shell=True)