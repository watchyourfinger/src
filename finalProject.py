#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import hashlib
from pyfingerprint.pyfingerprint import PyFingerprint
from twilio.rest import Client
import subprocess
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os.path


# Define GPIO to LCD mapping
LCD_RS = 26
LCD_E  = 19
LCD_D4 = 13
LCD_D5 = 6
LCD_D6 = 5
LCD_D7 = 11
buzzer = 21

# Define some device constants
LCD_WIDTH = 16    # Maximum characters per line
LCD_CHR = True
LCD_CMD = False

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line

# Timing constants
E_PULSE = 0.0005
E_DELAY = 0.0005

def main():
  # Main program block
  GPIO.setwarnings(False)
  GPIO.setmode(GPIO.BCM)     #BCM GPIO Numbers
  GPIO.setup(LCD_E, GPIO.OUT)
  GPIO.setup(LCD_RS, GPIO.OUT)
  GPIO.setup(LCD_D4, GPIO.OUT)
  GPIO.setup(LCD_D5, GPIO.OUT)
  GPIO.setup(LCD_D6, GPIO.OUT)
  GPIO.setup(LCD_D7, GPIO.OUT)

 #Setup Keypad
  MATRIX = [ [1,2,3,'A'],
           [4,5,6,'B'],
           [7,8,9,'C'],
           ['*',0,'#','D'] ]

  #GPIO Pin Setup
  ROW = [2,3,4,17]
  COL = [27,22,10,9]
  length = 4
  result = ""

  for j in range(4):
          GPIO.setup(COL[j], GPIO.OUT)
          GPIO.output(COL[j], 1)

  for i in range(4):
      GPIO.setup(ROW[i], GPIO.IN, pull_up_down = GPIO.PUD_UP)

  # Initialise display
  lcd_init()

  #Description
  lcd_string("Welcome to our",LCD_LINE_1)
  lcd_string("Final Project!",LCD_LINE_2)
  time.sleep(1.2)

    # Project Authors
               #0123456789ABCDEF
  lcd_string("By: Nick Patten",LCD_LINE_1)
  lcd_string("& Trevor Bernard",LCD_LINE_2)

  time.sleep(1.2)  # 2 second delay

    # Project Information
  lcd_string("Place a finger",LCD_LINE_1)
  lcd_string("on the scanner.",LCD_LINE_2)
  time.sleep(1.2)

  #call FingerPrint Scanner
  fingerPrint()

  #Password for Keypad
  password = "1A4B"
  length = len(password)

  #Password from keypad
  lcd_string("Enter Password", LCD_LINE_1)
  lcd_string("on 4x4 Keypad",LCD_LINE_2)
  result = check_keypad(length)

  #Password Check for Keypad
  if result == password:
      lcd_string("Correct Pin.",LCD_LINE_1)
      lcd_string("Unlocked!",LCD_LINE_2)
      sendMail()
  else:
      lcd_string("Incorrect Pin.",LCD_LINE_1)
      lcd_string(" ",LCD_LINE_2)
      sendMail()
      exit(1)

#FINGERPRINT SCANNER
def fingerPrint():
  f = PyFingerprint('/dev/ttyUSB0', 57600, 0xFFFFFFFF, 0x00000000)

  if ( f.verifyPassword() == False ):
      raise ValueError('The given fingerprint sensor password is wrong!')

  ## Wait that finger is read
  while ( f.readImage() == False ):
      pass
  ## Converts read image to characteristics and stores it in charbuffer 1
  f.convertImage(0x01)
  ## Searchs template
  result = f.searchTemplate()

  positionNumber = result[0]
  accuracyScore = result[1]

  if ( positionNumber == -1 ):
      lcd_string("No match found!", LCD_LINE_1)
      lcd_string(" ",LCD_LINE_2)
      time.sleep(1.2)
      exit(1)
  else:
      lcd_string("   Correct!",LCD_LINE_1)
      lcd_string(" Match found.",LCD_LINE_2)
      time.sleep(1.2)
      print('Found template at position #' + str(positionNumber))
      print('The accuracy score is: ' + str(accuracyScore))


    ## Loads the found template to charbuffer 1
  f.loadTemplate(positionNumber, 0x01)

    ## Downloads the characteristics of template loaded in charbuffer 1
  characterics = str(f.downloadCharacteristics(0x01)).encode('utf-8')

    ## Hashes characteristics of template
  print('SHA-2 hash of template: ' + hashlib.sha256(characterics).hexdigest())

#Send Email
def sendMail():

  #Varables for email
  email = 'kilat42@gmail.com'
  password = 'aymjhlqokyvxamlw'
  send_to_email = 'kilat42@gmail.com'
  subject = 'Enter email subject here'
  message = 'Enter message for body of the email here'
  file_location = 'Enter path location of file here'

  #code for camera operation
  cmd = 'raspistill -f -o camera1.jpg'
  subprocess.call(cmd, shell=True)

  #code for email message
  msg = MIMEMultipart()
  msg['From'] = email
  msg['To'] = send_to_email
  msg['Subject'] = subject

  msg.attach(MIMEText(message, 'plain'))

  # Setup the attachment and make readable
  filename = os.path.basename('/home/pi/camera1.jpg')
  attachment = open(filename, "rb")
  part = MIMEBase('application', 'octet-stream')
  part.set_payload(attachment.read())
  encoders.encode_base64(part)
  part.add_header('Content-Disposition', "attachment; filename= %s" % filename)

  # Attach the attachment to the MIMEMultipart object
  msg.attach(part)

  lcd_string("Sending...",LCD_LINE_1)
  lcd_string(" ", LCD_LINE_2)

  #Connect to email server
  server = smtplib.SMTP('smtp.gmail.com', 587)
  server.starttls()
  server.login(email, password)
  text = msg.as_string() # Convert the MIMEMultipart object to a string to send
  server.sendmail(email, send_to_email, text) #Send the email
  server.quit()

  # This program will send a text message the desired phone number
  account_sid = 'AC04b975cc8627f1fe8616ba3229128501'
  auth_token = 'a45a3fbac6ea4e129ee16adad2940d07'
  client = Client(account_sid, auth_token)

  message = client.messages \
                  .create(
                       body="Hello, Someone has accessed your account.",
                       from_='+13863127639',
                       to='+19166072795'
                   )

  print(message.sid)

#KEYPAD CODE
def check_keypad(length):
  ROW = [2,3,4,17]
  COL = [27,22,10,9]
  MATRIX = [ [1,2,3,'A'],
           [4,5,6,'B'],
           [7,8,9,'C'],
           ['*',0,'#','D'] ]
  result = ""
  while(True):
            for j in range(4):
                GPIO.output(COL[j],0)
                for i in range(4):
                    if GPIO.input(ROW[i]) == 0:
                        time.sleep(0.3)
                        print(MATRIX[i][j])
                        result = result + str(MATRIX[i][j])
                        time.sleep(0.3)
                        while(GPIO.input(ROW[i]) == 0):
                            pass

                GPIO.output(COL[j],1)
		#Exit Loop
		if len(result) >= length:
		     return result

def lcd_init():
 # Initialise display
  lcd_byte(0x33,LCD_CMD) # 110011 Initialize
  lcd_byte(0x32,LCD_CMD) # 110010 Initialize
  lcd_byte(0x06,LCD_CMD) # 000110 Cursor move direction
  lcd_byte(0x0C,LCD_CMD) # 001100 Display On,Cursor Off, Blink Off
  lcd_byte(0x28,LCD_CMD) # 101000 Data length, number of lines, font size
  lcd_byte(0x01,LCD_CMD) # 000001 Clear display
  time.sleep(E_DELAY)

def lcd_byte(bits, mode):
  # Send byte to data pins
  # bits = data
  # mode = True  for character
  #        False for command

  GPIO.output(LCD_RS, mode) # RS

  # High bits
  GPIO.output(LCD_D4, False)
  GPIO.output(LCD_D5, False)
  GPIO.output(LCD_D6, False)
  GPIO.output(LCD_D7, False)
  if bits&0x10==0x10:
    GPIO.output(LCD_D4, True)
  if bits&0x20==0x20:
    GPIO.output(LCD_D5, True)
  if bits&0x40==0x40:
    GPIO.output(LCD_D6, True)
  if bits&0x80==0x80:
    GPIO.output(LCD_D7, True)

  # Toggle 'Enable' pin
  lcd_toggle_enable()

  # Low bits
  GPIO.output(LCD_D4, False)
  GPIO.output(LCD_D5, False)
  GPIO.output(LCD_D6, False)
  GPIO.output(LCD_D7, False)
  if bits&0x01==0x01:
    GPIO.output(LCD_D4, True)
  if bits&0x02==0x02:
    GPIO.output(LCD_D5, True)
  if bits&0x04==0x04:
    GPIO.output(LCD_D6, True)
  if bits&0x08==0x08:
    GPIO.output(LCD_D7, True)

  # Toggle 'Enable' pin
  lcd_toggle_enable()

def lcd_toggle_enable():
  # Toggle enable
  time.sleep(E_DELAY)
  GPIO.output(LCD_E, True)
  time.sleep(E_PULSE)
  GPIO.output(LCD_E, False)
  time.sleep(E_DELAY)

def lcd_string(message,line):
  # Send string to display

  message = message.ljust(LCD_WIDTH," ")

  lcd_byte(line, LCD_CMD)

  for i in range(LCD_WIDTH):
    lcd_byte(ord(message[i]),LCD_CHR)

if __name__ == '__main__':

  try:
    main()
  except KeyboardInterrupt:
    pass
  finally:
    lcd_byte(0x01, LCD_CMD)
    lcd_string("    Goodbye!",LCD_LINE_1)
    GPIO.cleanup()